#!/bin/bash
# vim: et sts=4 sw=4

set -eu

DEFAULT_EXCLUDE="kali-archive-keyring kali-debtags kali-grant-root"

USAGE="Usage: $0 [OPTIONS] PATTERN...

Visualize kali-* packages in a graph. PATTERN is used to match packages from
src:kali-meta.

OPTIONS

  --datadir, -d DIR   Directory where to keep all data created (for debug).
  --exclude, -x PKGS  Space-separated list of packages to exclude.
  --format, -f FMT    Either svg or png (defaults to svg).
  --list [PATTERN]... Only list binary packages, no graph.
  --with-depends      Also show non-Kali Depends.
  --with-recommends   Also show non-Kali Recommends.

NOTES

  This script uses the APT cache (ie. /var/lib/apt/lists), so make sure to run
  it from an up-to-date Kali system (ie. run 'apt update' before using it).

  Alternatively, if you use eg. chdist to maintain APT trees for other distros,
  run as: APT_CONFIG=~/.chdist/kali-rolling/etc/apt/apt.conf $0 ...
  (adjust the path for your setup).

  The argument PATTERN is matched against kali-meta packages (extended regexp),
  so it can be either an exact package name (eg. kali-desktop-xfce) or a pattern
  (eg. desktop). If more than one arguments are passed, they are joined in one
  regexp: '(ARG1|ARG2|...|ARGN)'.

  By default, the following packages are excluded from the graph:
  $DEFAULT_EXCLUDE

HOW TO READ THE GRAPH

  Legend for the graphs:
  * Depends: blue lines
  * Recommends: black lines

EXAMPLES

  List all kali-meta packages:
    $0 --list
  List only desktop packages:
    $0 --list desktop
  Make graph for kali-linux-default:
    $0 kali-linux-default
  Make graph for kali-linux-default and kali-linux-large:
    $0 kali-linux-default kali-linux-large
  Or more simply:
    $0 'default|large'
  Make graph for all desktop packages:
    $0 desktop
  Make graph to see everything:
    $0 '.*'
"

# -- Parse arguments --------------------------------------

DATADIR=
EXCLUDE=
FORMAT=svg
LIST_ONLY=0
WITH_DEPENDS=0
WITH_RECOMMENDS=0

while [ $# -gt 0 ]; do
    case $1 in
        --datadir|-d) DATADIR=$2 && shift ;;
        --exclude|-x) EXCLUDE=$2 && shift ;;
        --format|-f) FORMAT=$2 && shift ;;
        --list) LIST_ONLY=1 ;;
        --with-depends) WITH_DEPENDS=1 ;;
        --with-recommends) WITH_RECOMMENDS=1 ;;
        -*) echo "$USAGE" >&2; exit 1 ;;
        *) break ;;
    esac
    shift
done

# Need at least one argument, except if --list
if [ $# -eq 0 ] && ! (( LIST_ONLY )); then
    echo "Need one argument at least!" >&2
    exit 1
fi

# Set EXCLUDE
if [ -z "$EXCLUDE" ]; then
    EXCLUDE=$DEFAULT_EXCLUDE
fi
EXCLUDE=$(echo "$EXCLUDE" | tr ' ' '\n')

# -- Ensure that required packages are installed ----------

MISSING_PACKAGES=
for pkg in dctrl-tools debtree m4 graphviz; do
    dpkg -s $pkg 2>/dev/null | grep -q "ok installed" && continue
    MISSING_PACKAGES+="$pkg "
done
if [ "$MISSING_PACKAGES" ]; then
    echo "Please install required packages with:" >&2
    echo "apt update && apt install -y $MISSING_PACKAGES" >&2
    exit 1
fi
unset MISSING_PACKAGES

# -- Work out which packages we want to work with ---------

_get_kali_metapackages_dir() {
    grep "^Package:" debian/control | awk '{print $2}' | sort -u
}

_get_kali_metapackages_apt() {
    # Don't do that! The Binary field displayed by showsrc is formatted
    # in a peculiar way, it goes back to a newline after a certain
    # number of characters, hence it breaks grep.
    #apt-cache showsrc kali-meta | grep "^Binary:" | cut -d: -f2 - \
    #| tr -d ' ' | sed -e "s/,$//" | tr , "\n" | sort -u
    # Do that instead:
    apt-cache showsrc kali-meta | grep-dctrl -n -s Package-List - \
    | sed '/^ *$/d' | awk '{print $1}' | sort -u
}

get_kali_metapackages() { _get_kali_metapackages_apt; }

# Transform arguments into a pattern (ie. 'ARG1 ARG2' -> '(ARG1|ARG2)'),
# then match against all Kali packages, to keep only the packages that
# we want to process.
ALL_PACKAGES=$(get_kali_metapackages)
PACKAGES=$ALL_PACKAGES
if [ $# -gt 0 ]; then
    PATTERN="($(echo "$@" | sed 's/ /|/g'))"
    PACKAGES=$(echo "$ALL_PACKAGES" | grep -E "$PATTERN" || :)
    if [ -z "$PACKAGES" ]; then
        echo "No match for pattern '$PATTERN', try again!" >&2
        exit 1
    fi
fi

# Create a list of packages that are not available (eg. kali-linux-arm
# is not available for amd64 architecture). Those packages will still
# appear on the graph, but we won't process their dependencies.
ALL_KNOWN_PACKAGES=$(apt-cache pkgnames | sort -u)
UNAVAILABLE_PACKAGES=$(for pkg in $PACKAGES; do \
    echo "$ALL_KNOWN_PACKAGES" | grep -q "^${pkg}$" || echo $pkg; done)

# -- If listing only, just print and we're done -----------

if (( LIST_ONLY )); then
    echo "$PACKAGES"
    exit 0
fi

# -- Get to work ------------------------------------------

echo "Matched:" $PACKAGES

# Create data directory
if [ "$DATADIR" ]; then
    OLDDIR=    # we won't come back
    [ -d $DATADIR ] || mkdir $DATADIR
else
    OLDDIR=$(pwd)
    DATADIR=$(mktemp -d)
    trap "rm -fr $DATADIR" EXIT
fi
cd $DATADIR

# For each package, compute the graph
for pkg in $PACKAGES; do
    # Unavailable packages are not processed, but we still want
    # to see it on the graph, so we create the dot file manually.
    if echo "$UNAVAILABLE_PACKAGES" | grep -qx "$pkg"; then
        echo "Skipping $pkg (unavailable)"
        cat << EOF > $pkg.dot
digraph "$pkg" {
  node [shape=box];
  "$pkg" [style="setlinewidth(2)"]
}
EOF
        continue
    fi

    echo "Processing $pkg ..."

    # Get PreDepends, Depends and Recommends, recursively
    recursive_depends=$(apt-cache depends --recurse --no-suggests \
        --no-conflicts --no-breaks --no-replaces --no-enhances $pkg \
        | sed -E "s/\|?(Pre)?(Depends|Recommends)://" \
        | sed "s/^ *//" | sed -e "s/^<//" -e "s/>$//" \
        | sort -u)
        #| cut -d: -f1 | sort -u)

    # Save the non-kali rec deps in a file
    echo "$recursive_depends" | grep -v ^kali- > $pkg.pkglist
    # Add the exlude list
    echo "$EXCLUDE" >> $pkg.pkglist

    # Compute the graph
    if (( WITH_RECOMMENDS )); then
        debtree $pkg -q --no-conflicts --endlist $pkg.pkglist > $pkg.dot
    elif (( WITH_DEPENDS )); then
        debtree $pkg -q --no-conflicts --no-recommends --endlist $pkg.pkglist > $pkg.dot
    else
        debtree $pkg -q --no-conflicts --no-recommends --skiplist $pkg.pkglist > $pkg.dot
    fi
done

# If we work with more than one package, we need to merge the graphs.
# It's all a bit magic, so here are the references:
# - merge dots: https://stackoverflow.com/a/53204252/776208
# - drop dup arrows: https://stackoverflow.com/a/48266515/776208
output=result
if [ $(echo $PACKAGES | wc -w) -gt 1 ]; then
    cat << 'EOF' > $output.m4
strict digraph {
define(`digraph',`subgraph')
EOF
    for pkg in $PACKAGES; do
        echo "include($pkg.dot)" >> $output.m4
    done
    echo "}" >> $output.m4
    m4 $output.m4 > $output.dot
else
    cp $pkg.dot $output.dot
fi

# Convert to svg
dot -T $FORMAT -o $output.$FORMAT $output.dot

# Copy back to initial working directory
if [ "$OLDDIR" ]; then
    cp $output.$FORMAT $OLDDIR
    echo Result: $output.$FORMAT
else
    echo Result: $DATADIR/$output.$FORMAT
fi
